Name:           mediastreamer2
Version:        4.5.16
Release:        5%{?dist}
Summary:        Streaming engine for voice/video telephony applications

License:        GPLv3+
URL:            https://github.com/BelledonneCommunications/%{name}
Source0:        https://github.com/BelledonneCommunications/%{name}/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz
# FIXME: send upstream
Patch0:         %{name}-portability.patch
# not intended for upstream
Patch1:         %{name}-rpath.patch

BuildRequires:  alsa-lib-devel
#XXX: BuildRequires:  bcg729-devel
BuildRequires:  bctoolbox-devel
BuildRequires:  bzrtp-devel
BuildRequires:  cmake
BuildRequires:  doxygen
BuildRequires:  ffmpeg-devel
BuildRequires:  glew-devel
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  gsm-devel
BuildRequires:  libpcap-devel
BuildRequires:  libv4l-devel
BuildRequires:  libvpx-devel
BuildRequires:  libX11-devel
BuildRequires:  libXv-devel
BuildRequires:  libsrtp-devel
BuildRequires:  libtheora-devel
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  opus-devel
BuildRequires:  ortp-devel
BuildRequires:  spandsp-devel
BuildRequires:  speex-devel
BuildRequires:  speexdsp-devel
BuildRequires:  turbojpeg-devel
# TODO: libbv16 (codec)
# TODO: libmatroska/bcmatroska2 + qnx (filtres)
# TODO: zxing (qrcode)

%description
Mediastreamer2 is a powerful and lightweight streaming engine for voice/video
telephony applications. This media processing and streaming toolkit is
responsible for receiving and sending all multimedia streams in Linphone,
including voice/video capture, encoding and decoding, and rendering.

For additional information, please visit
http://www.linphone.org/technical-corner/mediastreamer2.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -p1
rm -rf ortp-deps


%build
%cmake -DENABLE_STATIC=OFF -DENABLE_NON_FREE_CODECS=ON -DENABLE_PCAP=ON
%cmake_build


%install
%cmake_install
rm -rf %{buildroot}%{_docdir}/%{name}*/


%{?ldconfig_scriptlets}


%files
%license LICENSE.txt
%doc CHANGELOG.md
%doc README.md
%dir %{_datadir}/images/
%{_bindir}/mediastream
%{_bindir}/mkvstream
%{_datadir}/images/nowebcamCIF.jpg
%{_libdir}/libmediastreamer.so.11

%files devel
%doc %{_vpath_builddir}/help/doc/html/
%{_bindir}/mediastreamer2_tester
%{_datadir}/Mediastreamer2/
%{_datadir}/mediastreamer2_tester/
%{_includedir}/%{name}/
%{_libdir}/libmediastreamer.so


%changelog
* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.16-5
- Change license to GPLv3+

* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.16-4
- bzrtp BR

* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.16-3
- cmake BR

* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.16-2
- Fix build command

* Tue May 25 2021 František Dvořák <valtri@civ.zcu.cz> - 4.5.16-1
- Initial packaging
